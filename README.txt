
jQuery History Plugin

Installation:
- Download JS file from: http://www.mikage.to/jquery/jquery.history.js and place it in the jquery_history folder
- Enable module at admin -> build -> modules

General Use: 
To include in a module, use jquery_history_add(); 

Example: http://www.mikage.to/jquery/jquery_history.html
